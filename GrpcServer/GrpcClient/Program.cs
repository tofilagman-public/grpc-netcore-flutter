﻿using System;
using System.Threading.Tasks;
using GrpcServer;

namespace SamGrpcClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            using var channel = Grpc.Net.Client.GrpcChannel.ForAddress("http://localhost:7400");

            var client = new Greeter.GreeterClient(channel);
            var reply = await client.SayHelloAsync(new HelloRequest { Name = "hello" });

            Console.WriteLine("Greeting: " + reply.Message);
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}
