

# this is a working code for grpc netcore > flutter

## Flutter Setup

Install protoc_plugin on flutter
```bash
$ flutter pub global activate protoc_plugin
```

now go to FLUTTER_HOME/.pub-cache/bin and you will see the protoc-gen-dart.bat
we need to add this to PATH = FLUTTER_HOME/.pub-cache/bin
download the protoc on github https://github.com/protocolbuffers/protobuf/releases/tag/v3.14.0
make sure the exe or the executable binary you were using
extract it somewhere and add to PATH

now you were able to execute this command from Flutter terminal

```bash
$ protoc -I protos/ protos/greet.proto --dart_out=grpc:lib/src/generated
```

expose the port to adb

```bash
$ adb reverse tcp:5000 tcp:5000
```